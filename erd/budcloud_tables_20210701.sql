SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE IF EXISTS tb_c_comm_code;
DROP TABLE IF EXISTS tb_m_admin;
DROP TABLE IF EXISTS tb_m_admin_log;
DROP TABLE IF EXISTS tb_m_company;
DROP TABLE IF EXISTS tb_m_seat;
DROP TABLE IF EXISTS tb_m_seat_area;
DROP TABLE IF EXISTS tb_m_seat_use;
DROP TABLE IF EXISTS tb_m_seat_use_log;
DROP TABLE IF EXISTS tb_m_user;
DROP TABLE IF EXISTS tb_m_user_log;




/* Create Tables */

CREATE TABLE tb_c_comm_code
(
	id bigint NOT NULL AUTO_INCREMENT,
	code_id varchar(4) NOT NULL,
	code_nm varchar(32) NOT NULL,
	code_alias varchar(32),
	code_value varchar(255),
	group_id varchar(4) NOT NULL,
	group_nm varchar(32),
	priority int,
	use_yn varchar(1),
	upd_date datetime,
	PRIMARY KEY (id)
);


CREATE TABLE tb_m_admin
(
	id bigint NOT NULL AUTO_INCREMENT,
	-- email
	admin_id varchar(100) NOT NULL COMMENT 'email',
	admin_pw varchar(100) NOT NULL,
	admin_nm varchar(50) NOT NULL,
	admin_org varchar(50),
	admin_pos varchar(32),
	admin_mobile varchar(15),
	admin_phone varchar(20),
	comp_key varchar(100) NOT NULL,
	use_yn varchar(1),
	reg_date datetime,
	upd_date datetime,
	PRIMARY KEY (id),
	UNIQUE (admin_id)
);


CREATE TABLE tb_m_admin_log
(
	id bigint NOT NULL AUTO_INCREMENT,
	comp_key varchar(100),
	comp_nm varchar(50),
	-- email
	admin_id varchar(100) COMMENT 'email',
	admin_nm varchar(50),
	content text,
	ip_addr varchar(20),
	reg_date datetime,
	PRIMARY KEY (id)
);


CREATE TABLE tb_m_company
(
	id bigint NOT NULL AUTO_INCREMENT,
	comp_key varchar(100) NOT NULL,
	comp_nm varchar(50) NOT NULL,
	comp_domain varchar(50),
	user_limit bigint,
	seat_limit bigint,
	content text,
	use_yn varchar(1),
	reg_date datetime,
	upd_date datetime,
	PRIMARY KEY (id),
	UNIQUE (comp_key)
);


CREATE TABLE tb_m_seat
(
	id bigint NOT NULL AUTO_INCREMENT,
	area_id bigint NOT NULL,
	seat_no varchar(100) NOT NULL,
	fix_flag varchar(1),
	prohibition_flag varchar(1),
	coords varchar(100),
	seat_color varchar(10),
	mac_address varchar(100),
	comp_key varchar(100),
	upd_date datetime,
	PRIMARY KEY (id)
);


CREATE TABLE tb_m_seat_area
(
	id bigint NOT NULL AUTO_INCREMENT,
	parent_id varchar(200),
	area_nm varchar(50),
	plan_file varchar(200),
	comp_key varchar(100),
	use_yn varchar(1),
	upd_date datetime,
	PRIMARY KEY (id)
);


CREATE TABLE tb_m_seat_use
(
	comp_key varchar(100),
	-- tb_m_seat 의 id 와 FK
	seat_id bigint COMMENT 'tb_m_seat 의 id 와 FK',
	-- tb_m_user 의 id 와 FK
	user_id bigint NOT NULL COMMENT 'tb_m_user 의 id 와 FK',
	extension varchar(20),
	mac_address varchar(100),
	upd_date datetime
);


CREATE TABLE tb_m_seat_use_log
(
	id bigint NOT NULL AUTO_INCREMENT,
	comp_key varchar(100),
	seat_nm varchar(100),
	user_nm varchar(50),
	extension varchar(20),
	mac_address varchar(100),
	-- login/logout
	log_type varchar(20) COMMENT 'login/logout',
	-- admin/user
	user_type varchar(20) COMMENT 'admin/user',
	content text,
	upd_date datetime,
	PRIMARY KEY (id)
);


CREATE TABLE tb_m_user
(
	id bigint NOT NULL AUTO_INCREMENT,
	-- email
	user_id varchar(100) NOT NULL COMMENT 'email',
	user_pw varchar(100) NOT NULL,
	user_nm varchar(50),
	user_org varchar(50),
	user_pos varchar(50),
	user_mobile varchar(20),
	-- 뒤4자리는 extension 으로 사용
	user_phone varchar(20) COMMENT '뒤4자리는 extension 으로 사용',
	pic_path varchar(200),
	comp_key varchar(100),
	use_yn varchar(1),
	reg_date datetime,
	upd_date datetime,
	PRIMARY KEY (id),
	UNIQUE (user_id)
);


CREATE TABLE tb_m_user_log
(
	id bigint NOT NULL AUTO_INCREMENT,
	comp_key varchar(100),
	comp_nm varchar(50),
	-- 사용자인증키
	user_email varchar(50) COMMENT '사용자인증키',
	user_nm varchar(50),
	content text,
	ip_addr varchar(20),
	reg_date datetime,
	PRIMARY KEY (id)
);



