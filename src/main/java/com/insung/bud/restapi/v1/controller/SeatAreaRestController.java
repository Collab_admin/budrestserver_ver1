package com.insung.bud.restapi.v1.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.insung.bud.common.model.ParamModel;
import com.insung.bud.common.util.CommonUtil;
import com.insung.bud.restapi.v1.dao.SeatAreaDao;
import com.insung.bud.restapi.v1.model.SeatAreaForm;
import com.insung.bud.restapi.v1.model.SeatAreaModel;

@RestController
@RequestMapping("/restapi/v1")
@CrossOrigin("*")
public class SeatAreaRestController {
	 
	private static final Logger logger = LoggerFactory.getLogger(SeatAreaRestController.class);

	@Autowired
	SeatAreaDao seatAreaDao;
	
	//list
	@RequestMapping(method=RequestMethod.GET, path="/seatarea")
	public Map<String, Object> readAll(
			@RequestParam(value = "start", required = false, defaultValue = "") String start,
			@RequestParam(value = "length", required = false, defaultValue = "") String length,
			@RequestParam(value = "sidx", required = false, defaultValue = "") String sidx,
			@RequestParam(value = "sord", required = false, defaultValue = "") String sord,
			@RequestParam(value = "filter", required = false, defaultValue = "") String filter
		) {
		logger.debug("GET /seatarea");
		logger.debug("start : "+start);
		logger.debug("length : "+length);
		logger.debug("sidx : "+sidx);
		logger.debug("sord : "+sord);
		logger.debug("filter : "+filter);
		
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		
		ParamModel paramModel = new ParamModel();
		paramModel.setStart(start);
		paramModel.setLength(length);
		paramModel.setSidx(sidx);
		paramModel.setSord(sord);
		paramModel.setFilter(filter);
				
		try {
			
			int total = seatAreaDao.readCount(paramModel);
			List<SeatAreaModel> resultList = seatAreaDao.readAll(paramModel);		
			
			map.put("message", true);
			map.put("total", total);
			map.put("data", resultList);	
			
		} catch (Exception e) {
			map.put("message", false);
			logger.debug("Exception", e);
		}
		return map;
	}
	
	
	//detail
	@RequestMapping(method=RequestMethod.GET, path="/seatarea/{id}")
    public Map<String, Object> readOne(@PathVariable int id){
		logger.debug("GET /seatarea/{id}");
		logger.debug("id : "+id);
		
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		
		try {
			SeatAreaModel seatAreaModel = seatAreaDao.readOne(id);	
			
			map.put("message", true);
			map.put("data", seatAreaModel);	
		} catch (Exception e) {
			map.put("message", false);
			logger.debug("Exception", e);
		}
		return map;
    }
	
	
	//insert
	@RequestMapping(method=RequestMethod.POST, path="/seatarea")
	public Map<String, Object> create(@RequestBody SeatAreaForm seatAreaForm){
		logger.debug("POST /seatarea");		
		
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		
		try {	
			String reque_body = CommonUtil.getObjToStr(seatAreaForm);
			logger.debug("request body : "+ reque_body);			
			
			seatAreaDao.create(seatAreaForm);
					
			map.put("message", true);	
		} catch (Exception e) {
			map.put("message", false);
			logger.debug("Exception", e); 
		}
		return map;
	}
	
	
	//update
	@RequestMapping(method=RequestMethod.PUT, path="/seatarea/{id}")
	public Map<String, Object> update(@PathVariable int id, @RequestBody SeatAreaForm seatAreaForm) {
		logger.debug("PUT /seatarea/{id}");
		logger.debug("id : "+id);
		
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		
		try {
			String reque_body = CommonUtil.getObjToStr(seatAreaForm);
			logger.debug("request body : "+ reque_body);
			
			seatAreaDao.update(id, seatAreaForm);
			map.put("message", true);	
		} catch (Exception e) {
			map.put("message", false);
			logger.debug("Exception", e);
		}
		return map;
	}
	
	
	//delete
	@RequestMapping(method=RequestMethod.DELETE, path="/seatarea/{id}")
	public Map<String, Object> delete(@PathVariable int id) {
		logger.debug("DELETE /seatarea/{id}");
		logger.debug("id : "+id);
		
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		
		try { 
			seatAreaDao.delete(id);
			map.put("message", true);	
		} catch (Exception e) {
			map.put("message", false);
			logger.debug("Exception", e);
		}
		return map;
	}

}