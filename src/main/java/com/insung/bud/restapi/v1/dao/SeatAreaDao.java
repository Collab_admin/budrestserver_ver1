package com.insung.bud.restapi.v1.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.insung.bud.common.model.ParamModel;
import com.insung.bud.restapi.v1.model.SeatAreaForm;
import com.insung.bud.restapi.v1.model.SeatAreaModel;

@Repository @Transactional(value="transactionManager")
public class SeatAreaDao {

	@Autowired
    private SqlSession sqlSession; 
	
	//List Count
	public int readCount(ParamModel paramModel) throws Exception {
		return sqlSession.selectOne("SeatAreaDao.readCount", paramModel);		
	} 
	
	//List 
	public List<SeatAreaModel> readAll(ParamModel paramModel) throws Exception {
		return sqlSession.selectList("SeatAreaDao.readAll", paramModel);		
	}
	
	//Detail
	public SeatAreaModel readOne(int id) throws Exception {
		return sqlSession.selectOne("SeatAreaDao.readOne", id);	
	}	
	
	//Insert
	public void create(SeatAreaForm seatAreaForm) throws Exception {
		sqlSession.insert("SeatAreaDao.create", seatAreaForm);
	}
	
	//Update
	public void update(int id, SeatAreaForm seatAreaForm) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("item", seatAreaForm);
		sqlSession.update("SeatAreaDao.update", map);
	}
	
	//Delete
	public void delete(int id) throws Exception {
		sqlSession.delete("SeatAreaDao.delete", id);
	}
}
