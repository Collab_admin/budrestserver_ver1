package com.insung.bud.restapi.v1.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserModel {
	private int id;
	private String user_id;			
	private String user_nm;			
	private String user_org;			
	private String user_pos;			
	private String user_mobile;			
	private String user_phone;			
	private String pic_path;			
	private String comp_key;
	private String reg_date;
	private String upd_date;
}