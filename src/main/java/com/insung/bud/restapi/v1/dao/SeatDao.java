package com.insung.bud.restapi.v1.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.insung.bud.common.model.ParamModel;
import com.insung.bud.restapi.v1.model.SeatForm;
import com.insung.bud.restapi.v1.model.SeatModel;

@Repository @Transactional(value="transactionManager")
public class SeatDao {

	@Autowired
    private SqlSession sqlSession; 
	
	//List Count
	public int readCount(ParamModel paramModel) throws Exception {
		return sqlSession.selectOne("SeatDao.readCount", paramModel);		
	} 
	
	//List 
	public List<SeatModel> readAll(ParamModel paramModel) throws Exception {
		return sqlSession.selectList("SeatDao.readAll", paramModel);		
	}
	
	//Detail
	public SeatModel readOne(int id) throws Exception {
		return sqlSession.selectOne("SeatDao.readOne", id);	
	}	
	
	//Insert
	public void create(SeatForm seatForm) throws Exception {
		sqlSession.insert("SeatDao.create", seatForm);
	}
	
	//Update
	public void update(int id, SeatForm seatForm) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("item", seatForm);
		sqlSession.update("SeatDao.update", map);
	}
	
	//Delete
	public void delete(int id) throws Exception {
		sqlSession.delete("SeatDao.delete", id);
	}
}
