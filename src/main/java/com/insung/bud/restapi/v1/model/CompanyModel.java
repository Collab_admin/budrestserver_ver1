package com.insung.bud.restapi.v1.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CompanyModel {
	private int id;
	private String comp_key;
	private String comp_nm;
	private String comp_domain;
	private int user_limit;
	private int seat_limit;
	private String content;
	private String reg_date;
	private String upd_date;
}