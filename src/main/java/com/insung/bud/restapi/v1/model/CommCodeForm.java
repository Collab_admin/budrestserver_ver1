package com.insung.bud.restapi.v1.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CommCodeForm {
	private String code_id;
	private String code_nm;
	private String code_alias;
	private String code_value;
	private String group_id;
	private String group_nm;
	private int priority;
}