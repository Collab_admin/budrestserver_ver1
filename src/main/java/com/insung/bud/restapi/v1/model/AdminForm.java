package com.insung.bud.restapi.v1.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AdminForm {
	private String admin_id;			
	private String admin_nm;			
	private String admin_org;			
	private String admin_pos;			
	private String admin_mobile;			
	private String admin_phone;			
	private String comp_key;
}