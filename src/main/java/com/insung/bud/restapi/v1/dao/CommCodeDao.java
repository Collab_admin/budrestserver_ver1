package com.insung.bud.restapi.v1.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.insung.bud.common.model.ParamModel;
import com.insung.bud.restapi.v1.model.CommCodeForm;
import com.insung.bud.restapi.v1.model.CommCodeModel;

@Repository @Transactional(value="transactionManager")
public class CommCodeDao {

	@Autowired
    private SqlSession sqlSession; 
	
	//List Count
	public int readCount(ParamModel paramModel) throws Exception {
		return sqlSession.selectOne("CommCodeDao.readCount", paramModel);		
	} 
	
	//List 
	public List<CommCodeModel> readAll(ParamModel paramModel) throws Exception {
		return sqlSession.selectList("CommCodeDao.readAll", paramModel);		
	}
	
	//Detail
	public CommCodeModel readOne(int id) throws Exception {
		return sqlSession.selectOne("CommCodeDao.readOne", id);	
	}	
	
	//Insert
	public void create(CommCodeForm commCodeForm) throws Exception {
		sqlSession.insert("CommCodeDao.create", commCodeForm);
	}
	
	//Update
	public void update(int id, CommCodeForm commCodeForm) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("item", commCodeForm);
		sqlSession.update("CommCodeDao.update", map);
	}
	
	//Delete
	public void delete(int id) throws Exception {
		sqlSession.delete("CommCodeDao.delete", id);
	}
}
