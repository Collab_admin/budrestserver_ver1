package com.insung.bud.restapi.v1.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SeatModel {
	private int id;
	private int area_id;
	private String seta_no;
	private String fix_flag;
	private String prohibition_flag;
	private String coords;
	private String seat_color;
	private String mac_address;
	private String comp_key;
	private String upd_date;
}