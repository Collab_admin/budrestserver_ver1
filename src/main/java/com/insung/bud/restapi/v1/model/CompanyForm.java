package com.insung.bud.restapi.v1.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CompanyForm {
	private String comp_nm;
	private String comp_domain;
	private int user_limit;
	private int seat_limit;
	private String content;
}