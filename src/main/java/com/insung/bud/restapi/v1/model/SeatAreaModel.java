package com.insung.bud.restapi.v1.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SeatAreaModel {
	private int id;
	private String parent_id;
	private String area_nm;
	private String plan_file;
	private String comp_key;
	private String upd_date;
}