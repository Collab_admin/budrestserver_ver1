package com.insung.bud.restapi.v1.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.insung.bud.common.model.ParamModel;
import com.insung.bud.restapi.v1.model.UserForm;
import com.insung.bud.restapi.v1.model.UserModel;

@Repository @Transactional(value="transactionManager")
public class UserDao {

	@Autowired
    private SqlSession sqlSession; 
	
	//List Count
	public int readCount(ParamModel paramModel) throws Exception {
		return sqlSession.selectOne("UserDao.readCount", paramModel);		
	} 
	
	//List 
	public List<UserModel> readAll(ParamModel paramModel) throws Exception {
		return sqlSession.selectList("UserDao.readAll", paramModel);		
	}
	
	//Detail
	public UserModel readOne(int id) throws Exception {
		return sqlSession.selectOne("UserDao.readOne", id);	
	}	
	
	//Insert
	public void create(UserForm adminForm) throws Exception {
		sqlSession.insert("UserDao.create", adminForm);
	}
	
	//Update
	public void update(int id, UserForm adminForm) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("item", adminForm);
		sqlSession.update("UserDao.update", map);
	}
	
	//Delete
	public void delete(int id) throws Exception {
		sqlSession.delete("UserDao.delete", id);
	}
}
