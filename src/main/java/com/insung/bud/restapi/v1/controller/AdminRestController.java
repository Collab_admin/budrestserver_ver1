package com.insung.bud.restapi.v1.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.insung.bud.common.util.CommonUtil;
import com.insung.bud.restapi.v1.dao.AdminDao;
import com.insung.bud.restapi.v1.model.AdminForm;
import com.insung.bud.restapi.v1.model.AdminModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/restapi/v1")
@CrossOrigin("*")
public class AdminRestController {

	private static final Logger logger = LoggerFactory.getLogger(AdminRestController.class);

	@Autowired
	AdminDao adminDao;

	//list
	@RequestMapping(method = RequestMethod.GET, path = "/admins")
	public Map<String, Object> readAll(@RequestParam(value = "start", required = false, defaultValue = "") String start,
			@RequestParam(value = "length", required = false, defaultValue = "") String length,
			@RequestParam(value = "sidx", required = false, defaultValue = "") String sidx,
			@RequestParam(value = "sord", required = false, defaultValue = "") String sord,
			@RequestParam(value = "filter", required = false, defaultValue = "") String filter,
			@RequestParam(value = "comp_key", required = false, defaultValue = "") String comp_key) {
		logger.debug("GET /admins");
		logger.debug("start {} ", start);
		logger.debug("length {} ", length);
		logger.debug("sidx {} ", sidx);
		logger.debug("sord {} ", sord);
		logger.debug("filter {} ", filter);
		logger.debug("comp_key {} ", comp_key);

		Map<String, Object> map = new LinkedHashMap<String, Object>();
		map.put("start", start);
		map.put("length", length);
		map.put("sidx", sidx);
		map.put("sord", sord);
		map.put("filter", filter);
		map.put("comp_key", comp_key);

		try {
			int total = adminDao.readCount(map);
			List<AdminModel> resultList = adminDao.readAll(map);

			map.put("message", true);
			map.put("total", total);
			map.put("data", resultList);

		} catch (Exception e) {
			map.put("message", false);
			logger.debug("Exception", e);
		}
		return map;
	}

	
	//detail
	@RequestMapping(method = RequestMethod.GET, path = "/admins/{id}")
	public Map<String, Object> readOne(@PathVariable int id) {
		logger.debug("GET /admins/{id}");
		logger.debug("id {} ", id);

		Map<String, Object> map = new LinkedHashMap<String, Object>();

		try {
			AdminModel adminModel = adminDao.readOne(id);

			map.put("message", true);
			map.put("data", adminModel);
		} catch (Exception e) {
			map.put("message", false);
			logger.debug("Exception", e);
		}
		return map;
	}


	//insert
	@RequestMapping(method = RequestMethod.POST, path = "/admins")
	public Map<String, Object> create(@RequestBody AdminForm adminForm) {
		logger.debug("POST /Admins");

		Map<String, Object> map = new LinkedHashMap<String, Object>();

		try {
			String requestBody = CommonUtil.getObjToStr(adminForm);
			logger.debug("requestBody {} ", requestBody);

			adminDao.create(adminForm);

			map.put("message", true);
		} catch (Exception e) {
			map.put("message", false);
			logger.debug("Exception", e);
		}
		return map;
	}

	
	// update
	@RequestMapping(method = RequestMethod.PUT, path = "/admins/{id}")
	public Map<String, Object> update(@PathVariable int id, @RequestBody AdminForm adminForm) {
		logger.debug("PUT /admins/{id}");
		logger.debug("id {} ", id);

		Map<String, Object> map = new LinkedHashMap<String, Object>();

		try {
			String reque_body = CommonUtil.getObjToStr(adminForm);
			logger.debug("requestBody {} ", reque_body);

			adminDao.update(id, adminForm);
			map.put("message", true);
		} catch (Exception e) {
			map.put("message", false);
			logger.debug("Exception", e);
		}
		return map;
	}

	// delete
	@RequestMapping(method = RequestMethod.DELETE, path = "/admins/{id}")
	public Map<String, Object> delete(@PathVariable int id) {
		logger.debug("DELETE /admins/{id}");
		logger.debug("id {} ", id);

		Map<String, Object> map = new LinkedHashMap<String, Object>();

		try {
			adminDao.delete(id);
			map.put("message", true);
		} catch (Exception e) {
			map.put("message", false);
			logger.debug("Exception", e);
		}
		return map;
	}

}