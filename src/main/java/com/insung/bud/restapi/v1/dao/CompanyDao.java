package com.insung.bud.restapi.v1.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.insung.bud.common.model.ParamModel;
import com.insung.bud.restapi.v1.model.CompanyForm;
import com.insung.bud.restapi.v1.model.CompanyModel;

@Repository @Transactional(value="transactionManager")
public class CompanyDao {

	@Autowired
    private SqlSession sqlSession; 
	
	//List Count
	public int readCount(ParamModel paramModel) throws Exception {
		return sqlSession.selectOne("CompanyDao.readCount", paramModel);		
	} 
	
	//List 
	public List<CompanyModel> readAll(ParamModel paramModel) throws Exception {
		return sqlSession.selectList("CompanyDao.readAll", paramModel);		
	}
	
	//Detail
	public CompanyModel readOne(int id) throws Exception {
		return sqlSession.selectOne("CompanyDao.readOne", id);	
	}	
	
	//Insert
	public void create(CompanyForm companyForm) throws Exception {
		sqlSession.insert("CompanyDao.create", companyForm);
	}
	
	//Update
	public void update(int id, CompanyForm companyForm) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("item", companyForm);
		sqlSession.update("CompanyDao.update", map);
	}
	
	//Delete
	public void delete(int id) throws Exception {
		sqlSession.delete("CompanyDao.delete", id);
	}
}
