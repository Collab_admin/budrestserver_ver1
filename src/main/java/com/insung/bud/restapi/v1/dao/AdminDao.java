package com.insung.bud.restapi.v1.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.insung.bud.restapi.v1.model.AdminForm;
import com.insung.bud.restapi.v1.model.AdminModel;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(value = "transactionManager")
public class AdminDao {

	@Autowired
	private SqlSession sqlSession;

	// List Count
	public int readCount(Map<String, Object> map) throws Exception {
		return sqlSession.selectOne("AdminDao.readCount", map);
	}

	// List
	public List<AdminModel> readAll(Map<String, Object> map) throws Exception {
		return sqlSession.selectList("AdminDao.readAll", map);
	}

	// Detail
	public AdminModel readOne(int id) throws Exception {
		return sqlSession.selectOne("AdminDao.readOne", id);
	}

	// Insert
	public void create(AdminForm adminForm) throws Exception {
		sqlSession.insert("AdminDao.create", adminForm);
	}

	// Update
	public void update(int id, AdminForm adminForm) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("item", adminForm);
		sqlSession.update("AdminDao.update", map);
	}

	// Delete
	public void delete(int id) throws Exception {
		sqlSession.delete("AdminDao.delete", id);
	}
}
