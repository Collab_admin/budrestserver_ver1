package com.insung.bud.restapi.v1.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.insung.bud.common.model.ParamModel;
import com.insung.bud.common.util.CommonUtil;
import com.insung.bud.restapi.v1.dao.UserDao;
import com.insung.bud.restapi.v1.model.UserForm;
import com.insung.bud.restapi.v1.model.UserModel;

@RestController
@RequestMapping("/restapi/v1")
@CrossOrigin("*")
public class UserRestController {
	 
	private static final Logger logger = LoggerFactory.getLogger(UserRestController.class);

	@Autowired
	UserDao userDao;
	
	//list
	@RequestMapping(method=RequestMethod.GET, path="/users")
	public Map<String, Object> readAll(
			@RequestParam(value = "start", required = false, defaultValue = "") String start,
			@RequestParam(value = "length", required = false, defaultValue = "") String length,
			@RequestParam(value = "sidx", required = false, defaultValue = "") String sidx,
			@RequestParam(value = "sord", required = false, defaultValue = "") String sord,
			@RequestParam(value = "filter", required = false, defaultValue = "") String filter
		) {
		logger.debug("GET /users");
		logger.debug("start : "+start);
		logger.debug("length : "+length);
		logger.debug("sidx : "+sidx);
		logger.debug("sord : "+sord);
		logger.debug("filter : "+filter);
		
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		
		ParamModel paramModel = new ParamModel();
		paramModel.setStart(start);
		paramModel.setLength(length);
		paramModel.setSidx(sidx);
		paramModel.setSord(sord);
		paramModel.setFilter(filter);
				
		try {
			
			int total = userDao.readCount(paramModel);
			List<UserModel> resultList = userDao.readAll(paramModel);		
			
			map.put("message", true);
			map.put("total", total);
			map.put("data", resultList);	
			
		} catch (Exception e) {
			map.put("message", false);
			logger.debug("Exception", e);
		}
		return map;
	}
	
	
	//detail
	@RequestMapping(method=RequestMethod.GET, path="/users/{id}")
    public Map<String, Object> readOne(@PathVariable int id){
		logger.debug("GET /users/{id}");
		logger.debug("id : "+id);
		
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		
		try {
			UserModel userModel = userDao.readOne(id);	
			
			map.put("message", true);
			map.put("data", userModel);	
		} catch (Exception e) {
			map.put("message", false);
			logger.debug("Exception", e);
		}
		return map;
    }
	
	
	//insert
	@RequestMapping(method=RequestMethod.POST, path="/users")
	public Map<String, Object> create(@RequestBody UserForm userForm){
		logger.debug("POST /Users");		
		
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		
		try {	
			String reque_body = CommonUtil.getObjToStr(userForm);
			logger.debug("request body : "+ reque_body);			
			
			userDao.create(userForm);
					
			map.put("message", true);	
		} catch (Exception e) {
			map.put("message", false);
			logger.debug("Exception", e); 
		}
		return map;
	}
	
	
	//update
	@RequestMapping(method=RequestMethod.PUT, path="/users/{id}")
	public Map<String, Object> update(@PathVariable int id, @RequestBody UserForm userForm) {
		logger.debug("PUT /users/{id}");
		logger.debug("id : "+id);
		
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		
		try {
			String reque_body = CommonUtil.getObjToStr(userForm);
			logger.debug("request body : "+ reque_body);
			
			userDao.update(id, userForm);
			map.put("message", true);	
		} catch (Exception e) {
			map.put("message", false);
			logger.debug("Exception", e);
		}
		return map;
	}
	
	
	//delete
	@RequestMapping(method=RequestMethod.DELETE, path="/users/{id}")
	public Map<String, Object> delete(@PathVariable int id) {
		logger.debug("DELETE /users/{id}");
		logger.debug("id : "+id);
		
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		
		try { 
			userDao.delete(id);
			map.put("message", true);	
		} catch (Exception e) {
			map.put("message", false);
			logger.debug("Exception", e);
		}
		return map;
	}

}