package com.insung.bud.restapi.v1.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.insung.bud.common.model.ParamModel;
import com.insung.bud.common.util.CommonUtil;
import com.insung.bud.restapi.v1.dao.CompanyDao;
import com.insung.bud.restapi.v1.model.CompanyForm;
import com.insung.bud.restapi.v1.model.CompanyModel;

@RestController
@RequestMapping("/restapi/v1")
@CrossOrigin("*")
public class CompanyRestController {

	private static final Logger logger = LoggerFactory.getLogger(CompanyRestController.class);

	@Autowired
	CompanyDao companyDao;

	//list
	@RequestMapping(method = RequestMethod.GET, path = "/companys")
	public Map<String, Object> readAll(@RequestParam(value = "start", required = false, defaultValue = "") String start,
			@RequestParam(value = "length", required = false, defaultValue = "") String length,
			@RequestParam(value = "sidx", required = false, defaultValue = "") String sidx,
			@RequestParam(value = "sord", required = false, defaultValue = "") String sord,
			@RequestParam(value = "filter", required = false, defaultValue = "") String filter) {
		logger.debug("GET /companys");
		logger.debug("start : {} ", start);
		logger.debug("length : {} ", length);
		logger.debug("sidx : {} ", sidx);
		logger.debug("sord : {} ", sord);
		logger.debug("filter : {} ", filter);

		Map<String, Object> map = new LinkedHashMap<String, Object>();

		ParamModel paramModel = new ParamModel();
		paramModel.setStart(start);
		paramModel.setLength(length);
		paramModel.setSidx(sidx);
		paramModel.setSord(sord);
		paramModel.setFilter(filter);

		try {

			int total = companyDao.readCount(paramModel);
			List<CompanyModel> resultList = companyDao.readAll(paramModel);

			map.put("message", true);
			map.put("total", total);
			map.put("data", resultList);

		} catch (Exception e) {
			map.put("message", false);
			logger.debug("Exception", e);
		}
		return map;
	}

	
	//detail
	@RequestMapping(method = RequestMethod.GET, path = "/companys/{id}")
	public Map<String, Object> readOne(@PathVariable int id) {
		logger.debug("GET /companys/{id}");
		logger.debug("id : {} ", id);

		Map<String, Object> map = new LinkedHashMap<String, Object>();

		try {
			CompanyModel CompanyModel = companyDao.readOne(id);

			map.put("message", true);
			map.put("data", CompanyModel);
		} catch (Exception e) {
			map.put("message", false);
			logger.debug("Exception", e);
		}
		return map;
	}

	
	// insert
	@RequestMapping(method = RequestMethod.POST, path = "/companys")
	public Map<String, Object> create(@RequestBody CompanyForm companyForm) {
		logger.debug("POST /companys");

		Map<String, Object> map = new LinkedHashMap<String, Object>();

		try {
			String requestBody = CommonUtil.getObjToStr(companyForm);
			logger.debug("requestBody : {} ", requestBody);

			companyDao.create(companyForm);

			map.put("message", true);
		} catch (Exception e) {
			map.put("message", false);
			logger.debug("Exception", e);
		}
		return map;
	}

	
	//update
	@RequestMapping(method = RequestMethod.PUT, path = "/companys/{id}")
	public Map<String, Object> update(@PathVariable int id, @RequestBody CompanyForm companyForm) {
		logger.debug("PUT /companys/{id}");
		logger.debug("id : {} ", id);

		Map<String, Object> map = new LinkedHashMap<String, Object>();

		try {
			String requestBody = CommonUtil.getObjToStr(companyForm);
			logger.debug("requestBody : {} ", requestBody);

			companyDao.update(id, companyForm);
			map.put("message", true);
		} catch (Exception e) {
			map.put("message", false);
			logger.debug("Exception", e);
		}
		return map;
	}

	
	//delete
	@RequestMapping(method = RequestMethod.DELETE, path = "/companys/{id}")
	public Map<String, Object> delete(@PathVariable int id) {
		logger.debug("DELETE /companys/{id}");
		logger.debug("id : {} ", id);

		Map<String, Object> map = new LinkedHashMap<String, Object>();

		try {
			companyDao.delete(id);
			map.put("message", true);
		} catch (Exception e) {
			map.put("message", false);
			logger.debug("Exception", e);
		}
		return map;
	}

}