package com.insung.bud.common.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ParamModel {
	private String start;
	private String length;
	private String sidx;
	private String sord;
	private String filter;
}