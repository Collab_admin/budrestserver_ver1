package com.insung;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BudRestServerV1Application {

	public static void main(String[] args) {
		SpringApplication.run(BudRestServerV1Application.class, args);
	}

}
