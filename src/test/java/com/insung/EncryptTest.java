package com.insung;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import com.insung.bud.common.util.CommonUtil;;

public class EncryptTest { 
	
	public static void main(String[] args){
		try {
			System.out.println(CommonUtil.encryptSHA256("인성정보"));
			//System.out.println(encryptMD5("!Insung2018#"));
			//System.out.println(encryptSHA256("!Insung2018#"));
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static String encryptMD5(String msg) throws NoSuchAlgorithmException {
	    MessageDigest md = MessageDigest.getInstance("MD5");
	    md.update(msg.getBytes());
	    byte byteData[] = md.digest();
		StringBuffer sb = new StringBuffer(); 
		for(int i=0; i<byteData.length; i++){
			sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}


	public static String encryptSHA256(String msg)  throws NoSuchAlgorithmException {
	    MessageDigest md = MessageDigest.getInstance("SHA-256");
	    md.update(msg.getBytes());
	    byte byteData[] = md.digest();
		StringBuffer sb = new StringBuffer(); 
		for(int i=0; i<byteData.length; i++){
			sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}


}