USE budcloud;
DROP PROCEDURE IF EXISTS procCommCodeSelectOne;

DELIMITER //
CREATE PROCEDURE procCommCodeSelectOne (
	IN $id INT
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	SELECT
		id,
		code_id,
		code_nm,
		code_alias,
		code_value,
		group_id,
		group_nm,
		priority,
		DATE_FORMAT(upd_date,'%Y-%m-%d %H:%i:%s') upd_date
	FROM tb_c_comm_code
	WHERE id=$id;	  
END;
//
DELIMITER ;

