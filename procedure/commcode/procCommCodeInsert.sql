USE budcloud;
DROP PROCEDURE IF EXISTS procCommCodeInsert;

DELIMITER //
CREATE PROCEDURE procCommCodeInsert (
	IN $code_id VARCHAR(4),
	IN $code_nm VARCHAR(32),
	IN $code_alias VARCHAR(32),
	IN $code_value VARCHAR(255),
	IN $group_id VARCHAR(4),
	IN $group_nm VARCHAR(32),
	IN $priority INT
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	INSERT INTO tb_c_comm_code (
		code_id,
		code_nm,
		code_alias,
		code_value,
		group_id,
		group_nm,
		priority,
		use_yn,
		upd_date
	) VALUES (
		$code_id,
		$code_nm,
		$code_alias,
		$code_value,
		$group_id,
		$group_nm,
		$priority,		
		'Y',
		NOW()
	);
END;
//
DELIMITER ;

