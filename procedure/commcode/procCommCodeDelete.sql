select * FROM tb_m_seat_area


SELECT AUTO_INCREMENT abc FROM tb_m_seat_area


SELECT case when LAST_INSERT_ID()='' then 0 ELSE 1 end abc FROM tb_m_seat_area


SELECT IFNULL(MAX(id),1) FROM tb_m_seat_area


CALL procSeatAreaSelectOne(1)


SELECT
	T.id,			
	admin_id,			
	admin_nm,			
	admin_org,			
	admin_pos,			
	admin_mobile,			
	admin_phone,	
	T.comp_key,
	TC.comp_nm, 			
	DATE_FORMAT(T.reg_date,'%Y-%m-%d %H:%i:%s') reg_date,
	DATE_FORMAT(T.upd_date,'%Y-%m-%d %H:%i:%s') upd_date,
	T.use_yn
FROM tb_m_admin T
INNER JOIN tb_m_company TC ON T.comp_key=TC.comp_key
WHERE T.use_yn='Y'
ORDER BY admin_id