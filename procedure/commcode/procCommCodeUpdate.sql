USE budcloud;
DROP PROCEDURE IF EXISTS procCommCodeUpdate;

DELIMITER //
CREATE PROCEDURE procCommCodeUpdate (
	IN $id INT,
	IN $code_id VARCHAR(4),
	IN $code_nm VARCHAR(32),
	IN $code_alias VARCHAR(32),
	IN $code_value VARCHAR(255),
	IN $group_id VARCHAR(4),
	IN $group_nm VARCHAR(32),
	IN $priority INT
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	UPDATE tb_c_comm_code SET
		code_id=$code_id,	  
		code_nm=$code_nm,	  
		code_alias=$code_alias,	  
		code_value=$code_value,	  
		group_id=$group_id,	  
		group_nm=$group_nm,	  
		priority=$priority,  
		upd_date=NOW()
	WHERE id=$id;		
END;
//
DELIMITER ;