USE budcloud;
DROP PROCEDURE IF EXISTS procCommCodeSelectCount;

DELIMITER //
CREATE PROCEDURE procCommCodeSelectCount (
	IN $filter VARCHAR(100)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	DECLARE tempQuery VARCHAR(1000);
	DECLARE whereCondition VARCHAR(500);

	IF $filter='' then
		SET whereCondition = '';
	ELSE
		SET whereCondition = CONCAT('AND ( code_nm like \'%', replace($filter,'%','\%'), '%\'');
		SET whereCondition = CONCAT(whereCondition, 'OR code_alias like \'%', replace($filter,'%','\%'), '%\'');
		SET whereCondition = CONCAT(whereCondition, 'OR code_value like \'%', replace($filter,'%','\%'), '%\'');
		SET whereCondition = CONCAT(whereCondition, 'OR group_id like \'%', replace($filter,'%','\%'), '%\'');
		SET whereCondition = CONCAT(whereCondition, 'OR group_nm like \'%', replace($filter,'%','\%'), '%\' )');
	END IF;	
	
	
	SET tempQuery = CONCAT ("	
		SELECT
			COUNT(*) total
		FROM tb_c_comm_code
		WHERE use_yn='Y' "
		, whereCondition
	);
	
	PREPARE sqlstmt FROM tempQuery;
	EXECUTE sqlstmt;	  
END;
//
DELIMITER ;

