USE budcloud;
DROP PROCEDURE IF EXISTS procSeatInsert;

DELIMITER //
CREATE PROCEDURE procSeatInsert (
	IN $area_id int,	
	IN $seat_no VARCHAR(100),
	IN $fix_flag VARCHAR(1),
	IN $prohibition_flag VARCHAR(1),
	IN $coords VARCHAR(100),
	IN $seat_color VARCHAR(10),
	IN $mac_address VARCHAR(100),
	IN $comp_key VARCHAR(100)	
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	INSERT INTO tb_m_seat (
		area_id,			
		seat_no,
		fix_flag,			
		prohibition_flag,
		coords,
		seat_color,
		mac_address,
		comp_key,
		upd_date
	) VALUES (
		$area_id,	
		$seat_no,			
		$fix_flag,			
		$prohibition_flag,
		$coords,
		$seat_color,
		$mac_address,
		$comp_key,
		NOW()
	);
END;
//
DELIMITER ;

