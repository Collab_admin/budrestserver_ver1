USE budcloud;
DROP PROCEDURE IF EXISTS procSeatSelectOne;

DELIMITER //
CREATE PROCEDURE procSeatSelectOne (
	IN $id INT
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	SELECT
		id,			
		area_id,						
		fix_flag,			
		prohibition_flag,	
		coords,
		seat_color,
		mac_address,
		comp_key,			
		DATE_FORMAT(upd_date,'%Y-%m-%d %H:%i:%s') upd_date
	FROM tb_m_seat
	WHERE id=$id;	  
END;
//
DELIMITER ;

