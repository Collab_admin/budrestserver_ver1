USE budcloud;
DROP PROCEDURE IF EXISTS procSeatSelectCount;

DELIMITER //
CREATE PROCEDURE procSeatSelectCount (
	IN $filter VARCHAR(100)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	DECLARE tempQuery VARCHAR(1000);
	DECLARE whereCondition VARCHAR(500);

	IF $filter='' then
		SET whereCondition = '';
	ELSE
		SET whereCondition = CONCAT('AND ( fix_flag like \'%', replace($filter,'%','\%'), '%\'');
		SET whereCondition = CONCAT(whereCondition, 'OR prohibition_flag like \'%', replace($filter,'%','\%'), '%\' )');
	END IF;	
	
	
	SET tempQuery = CONCAT ("	
		SELECT
			COUNT(*) total
		FROM tb_m_seat "
		, whereCondition
	);
	
	PREPARE sqlstmt FROM tempQuery;
	EXECUTE sqlstmt;	  
END;
//
DELIMITER ;

