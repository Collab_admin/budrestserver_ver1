USE budcloud;
DROP PROCEDURE IF EXISTS procSeatUpdate;

DELIMITER //
CREATE PROCEDURE procSeatUpdate (
	IN $area_id int,	
	IN $seat_no VARCHAR(100),
	IN $fix_flag VARCHAR(1),
	IN $prohibition_flag VARCHAR(1),
	IN $coords VARCHAR(100),
	IN $seat_color VARCHAR(10),
	IN $mac_address VARCHAR(100),
	IN $comp_key VARCHAR(100)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	UPDATE tb_m_seat SET
		area_id=$area_id,	  
		seat_no=$seat_no,
		fix_flag=$fix_flag,
		prohibition_flag=$prohibition_flag,
		coords=$coords,
		seat_color=$seat_color,
		$mac_address=$mac_address,
		$comp_key=$comp_key,
		upd_date=NOW()
	WHERE id=_id
	AND comp_key=_comp_key;		
END;
//
DELIMITER ;
