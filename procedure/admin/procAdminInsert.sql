USE budcloud;
DROP PROCEDURE IF EXISTS procAdminInsert;

DELIMITER //
CREATE PROCEDURE procAdminInsert (
	IN $admin_id VARCHAR(100),
	IN $admin_nm VARCHAR(50),
	IN $admin_org VARCHAR(50),
	IN $admin_pos VARCHAR(32),
	IN $admin_mobile VARCHAR(15),
	IN $admin_phone VARCHAR(20),
	IN $comp_key VARCHAR(100)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	INSERT INTO tb_m_admin (
		admin_id,			
		admin_pw,			
		admin_nm,			
		admin_org,			
		admin_pos,			
		admin_mobile,			
		admin_phone,			
		comp_key,
		use_yn,
		reg_date, 
		upd_date
	) VALUES (
		$admin_id,
		SHA2(CONCAT($admin_id, NOW()), 256),
		$admin_nm,
		$admin_org,
		$admin_pos,
		$admin_mobile,
		$admin_phone,
		$comp_key,
		'Y',
		NOW(),
		NOW()
	);
END;
//
DELIMITER ;

