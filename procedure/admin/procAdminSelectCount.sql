USE budcloud;
DROP PROCEDURE IF EXISTS procAdminSelectCount;

DELIMITER //
CREATE PROCEDURE procAdminSelectCount (
	IN $filter VARCHAR(100),
	IN $comp_key VARCHAR(100)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	DECLARE tempQuery VARCHAR(1000);
	DECLARE whereCondition VARCHAR(500);

	IF $filter='' then
		SET whereCondition = '';
	ELSE
		SET whereCondition = CONCAT('AND ( admin_nm like \'%', replace($filter,'%','\%'), '%\'');
		SET whereCondition = CONCAT(whereCondition, 'OR admin_mobile like \'%', replace($filter,'%','\%'), '%\' )');
	END IF;	
	
	IF $comp_key != '' then
		SET whereCondition = CONCAT(whereCondition , 'AND T.comp_key=\'', $comp_key, '\'');
	END IF;
	
	SET tempQuery = CONCAT ("	
		SELECT
			COUNT(*) total
		FROM tb_m_admin T
		INNER JOIN tb_m_company TC ON T.comp_key=TC.comp_key
		WHERE T.use_yn='Y'  "
		, whereCondition
	);
	
	PREPARE sqlstmt FROM tempQuery;
	EXECUTE sqlstmt;	  
END;
//
DELIMITER ;

