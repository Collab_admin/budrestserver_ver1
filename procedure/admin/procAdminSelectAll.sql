USE budcloud;
DROP PROCEDURE IF EXISTS procAdminSelectAll;

DELIMITER //
CREATE PROCEDURE procAdminSelectAll (
	IN $start VARCHAR(10), 
	IN $length VARCHAR(4),
	IN $sidx VARCHAR(20),
	IN $sord VARCHAR(5),
	IN $filter VARCHAR(100),
	IN $comp_key VARCHAR(100)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	DECLARE tempQuery VARCHAR(1000);
	DECLARE tempOrder VARCHAR(100);
	DECLARE tempLimit VARCHAR(100);
	DECLARE whereCondition VARCHAR(500);

	IF $sidx='' then
		SET tempOrder = ' ORDER BY id ASC ';
	ELSE
		SET tempOrder = CONCAT(' ORDER BY ', $sidx, ' ', $sord);
	END IF;

	IF $start='' then
		SET tempLimit = '';
	ELSE
		SET tempLimit = CONCAT(' LIMIT ', $length, ' OFFSET ', $start);
	END IF;	
	
	IF $filter='' then
		SET whereCondition = '';
	ELSE
		SET whereCondition = CONCAT('AND ( admin_nm like \'%', replace($filter,'%','\%'), '%\'');
		SET whereCondition = CONCAT(whereCondition, 'OR admin_mobile like \'%', replace($filter,'%','\%'), '%\' )');
	END IF;
	
	IF $comp_key != '' then
		SET whereCondition = CONCAT(whereCondition , 'AND T.comp_key=\'', $comp_key, '\'');
	END IF;
	
	SET tempQuery = CONCAT ("	
		SELECT
			T.id,			
			admin_id,			
			admin_nm,			
			admin_org,			
			admin_pos,			
			admin_mobile,			
			admin_phone,	
			T.comp_key,
			TC.comp_nm, 			
			DATE_FORMAT(T.reg_date,'%Y-%m-%d %H:%i:%s') reg_date,
			DATE_FORMAT(T.upd_date,'%Y-%m-%d %H:%i:%s') upd_date
		FROM tb_m_admin T
		INNER JOIN tb_m_company TC ON T.comp_key=TC.comp_key
		WHERE T.use_yn='Y'  "
		, whereCondition
	    , tempOrder
		, tempLimit
	);
	
	PREPARE sqlstmt FROM tempQuery;
	EXECUTE sqlstmt;	  
END;
//
DELIMITER ;

