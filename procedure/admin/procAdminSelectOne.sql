USE budcloud;
DROP PROCEDURE IF EXISTS procAdminSelectOne;

DELIMITER //
CREATE PROCEDURE procAdminSelectOne (
	IN $id INT
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	SELECT
		T.id,		
		admin_id,			
		admin_pw,			
		admin_nm,			
		admin_org,			
		admin_pos,			
		admin_mobile,			
		admin_phone,
		T.comp_key,
		TC.comp_nm,
		DATE_FORMAT(T.reg_date,'%Y-%m-%d %H:%i:%s') reg_date,
		DATE_FORMAT(T.upd_date,'%Y-%m-%d %H:%i:%s') upd_date
	FROM tb_m_admin T
	INNER JOIN tb_m_company TC ON T.comp_key=TC.comp_key
	WHERE T.use_yn='Y'
	AND T.id=$id;	  
END;
//
DELIMITER ;

