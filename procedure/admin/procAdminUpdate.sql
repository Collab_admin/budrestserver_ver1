USE budcloud;
DROP PROCEDURE IF EXISTS procAdminUpdate;

DELIMITER //
CREATE PROCEDURE procAdminUpdate (
	IN $id INT,
	IN $admin_nm VARCHAR(50),
	IN $admin_org VARCHAR(50),
	IN $admin_pos VARCHAR(32),
	IN $admin_mobile VARCHAR(15),
	IN $admin_phone VARCHAR(20)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	UPDATE tb_m_admin SET
		admin_nm=$admin_nm,	  
		admin_org=$admin_org,
		admin_pos=$admin_pos,
		admin_mobile=$admin_mobile,  
		admin_phone=$admin_phone,
		upd_date=NOW()
	WHERE id=$id;
END;
//
DELIMITER ;
