USE budcloud;
DROP PROCEDURE IF EXISTS procAdminDelete;

DELIMITER //
CREATE PROCEDURE procAdminDelete (
	IN $id INT
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN	
	UPDATE tb_m_admin SET
		admin_id=CONCAT((SELECT admin_id FROM tb_m_admin WHERE id=$id), '-', NOW()),
		use_yn='N',  
		upd_date=NOW()
	WHERE id=$id;		
END;
//
DELIMITER ;

