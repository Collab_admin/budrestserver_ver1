USE budcloud;
DROP PROCEDURE IF EXISTS procSeatAreaSelectAll;

DELIMITER //
CREATE PROCEDURE procSeatAreaSelectAll (
	IN $start VARCHAR(10), 
	IN $length VARCHAR(4),
	IN $sidx VARCHAR(20),
	IN $sord VARCHAR(5),
	IN $filter VARCHAR(100)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	DECLARE tempQuery VARCHAR(1000);
	DECLARE tempOrder VARCHAR(100);
	DECLARE tempLimit VARCHAR(100);
	DECLARE whereCondition VARCHAR(500);

	IF $sidx='' then
		SET tempOrder = ' ORDER BY id ASC ';
	ELSE
		SET tempOrder = CONCAT(' ORDER BY ', $sidx, ' ', $sord);
	END IF;

	IF $start='' then
		SET tempLimit = '';
	ELSE
		SET tempLimit = CONCAT(' LIMIT ', $length, ' OFFSET ', $start);
	END IF;	
	
	IF $filter='' then
		SET whereCondition = '';
	ELSE
		SET whereCondition = CONCAT('AND ( area_nm like \'%', replace($filter,'%','\%'), '%\'');
		SET whereCondition = CONCAT(whereCondition, 'OR plan_file like \'%', replace($filter,'%','\%'), '%\' )');
	END IF;	
	
	
	SET tempQuery = CONCAT ("	
		SELECT
			id,			
			parent_id,						
			area_nm,			
			plan_file,	
			comp_key,			
			DATE_FORMAT(upd_date,'%Y-%m-%d %H:%i:%s') upd_date
		FROM tb_m_seat_area
		WHERE use_yn='Y' "
		, whereCondition
	   , tempOrder
		, tempLimit
	);
	
	PREPARE sqlstmt FROM tempQuery;
	EXECUTE sqlstmt;	  
END;
//
DELIMITER ;

