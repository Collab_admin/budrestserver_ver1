USE budcloud;
DROP PROCEDURE IF EXISTS procSeatAreaInsert;

DELIMITER //
CREATE PROCEDURE procSeatAreaInsert (
	IN $parent_id VARCHAR(200),
	IN $area_nm VARCHAR(50),
	IN $plan_file VARCHAR(200),
	IN $comp_key VARCHAR(100)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	INSERT INTO tb_m_seat_area (
		parent_id,			
		area_nm,			
		plan_file,			
		comp_key,
		use_yn,
		upd_date
	) VALUES (
		$parent_id,
		$area_nm,
		$plan_file,
		$comp_key,
		'Y',
		NOW()
	);
END;
//
DELIMITER ;

