USE budcloud;
DROP PROCEDURE IF EXISTS procSeatAreaSelectOne;

DELIMITER //
CREATE PROCEDURE procSeatAreaSelectOne (
	IN $id INT
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	SELECT
		id,			
		parent_id,						
		area_nm,			
		plan_file,	
		comp_key,	 		
		DATE_FORMAT(upd_date,'%Y-%m-%d %H:%i:%s') upd_date
	FROM tb_m_seat_area
	WHERE id=$id;	  
END;
//
DELIMITER ;

