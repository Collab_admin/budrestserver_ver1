USE budcloud;
DROP PROCEDURE IF EXISTS procSeatAreaUpdate;

DELIMITER //
CREATE PROCEDURE procSeatAreaUpdate (
	IN $id INT,
	IN $parent_id VARCHAR(200),
	IN $area_nm VARCHAR(50),
	IN $plan_file VARCHAR(200),
	IN $comp_key VARCHAR(100)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	UPDATE tb_m_seat_area SET
		parent_id=$parent_id,	  
		area_nm=$area_nm,
		plan_file=$plan_file,
		upd_date=NOW()
	WHERE id=$id
	AND comp_key=$comp_key;		
END;
//
DELIMITER ;
