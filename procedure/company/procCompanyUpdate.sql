USE budcloud;
DROP PROCEDURE IF EXISTS procCompanyUpdate;

DELIMITER //
CREATE PROCEDURE procCompanyUpdate (
	IN $id INT,
	IN $comp_nm VARCHAR(50),
	IN $comp_domain VARCHAR(32),
	IN $user_limit INT,
	IN $seat_limit INT,
	IN $content TEXT
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	UPDATE tb_m_company SET
		comp_nm=$comp_nm,	  
		comp_domain=$comp_domain,	 
		user_limit=$user_limit,
		seat_limit=$seat_limit,
		content=$content,  
		upd_date=NOW()
	WHERE id=$id;		
END;
//
DELIMITER ;
