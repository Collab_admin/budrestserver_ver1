USE budcloud;
DROP PROCEDURE IF EXISTS procCompanySelectOne;

DELIMITER //
CREATE PROCEDURE procCompanySelectOne (
	IN $id INT
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	SELECT
		id,
		comp_key,
		comp_nm,
		comp_domain,
		user_limit,
		seat_limit,
		content,
		DATE_FORMAT(reg_date,'%Y-%m-%d %H:%i:%s') reg_date,
		DATE_FORMAT(upd_date,'%Y-%m-%d %H:%i:%s') upd_date
	FROM tb_m_company
	WHERE id=$id;	  
END;
//
DELIMITER ;

