USE budcloud;
DROP PROCEDURE IF EXISTS procCompanyInsert;

DELIMITER //
CREATE PROCEDURE procCompanyInsert (
	IN $comp_nm VARCHAR(50),
	IN $comp_domain VARCHAR(50),
	IN $user_limit INT,
	IN $seat_limit INT,
	IN $content TEXT
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	INSERT INTO tb_m_company (
		comp_key,
		comp_nm,
		comp_domain,
		user_limit,
		seat_limit,
		content,
		use_yn,
		reg_date, 
		upd_date
	) VALUES (
		SHA2(CONCAT($comp_nm, NOW()), 256),
		$comp_nm,
		$comp_domain,
		$user_limit,
		$seat_limit,
		$content,	
		'Y',
		NOW(),
		NOW()
	);
END;
//
DELIMITER ;

