USE budcloud;
DROP PROCEDURE IF EXISTS procUserInsert;

DELIMITER //
CREATE PROCEDURE procUserInsert (
	IN $user_id VARCHAR(100),
	IN $user_nm VARCHAR(50),
	IN $user_org VARCHAR(50),
	IN $user_pos VARCHAR(32),
	IN $user_mobile VARCHAR(15),
	IN $user_phone VARCHAR(20),
	IN $pic_path VARCHAR(200),
	IN $comp_key VARCHAR(100)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	INSERT INTO tb_m_user (
		user_id,			
		user_pw,			
		user_nm,			
		user_org,			
		user_pos,			
		user_mobile,			
		user_phone,			
		pic_path,			
		comp_key,
		use_yn,
		reg_date, 
		upd_date
	) VALUES (
		$user_id,
		SHA2(CONCAT($user_id, NOW()), 256),
		$user_nm,
		$user_org,
		$user_pos,
		$user_mobile,
		$user_phone,
		$pic_path,
		$comp_key,
		'Y',
		NOW(),
		NOW()
	);
END;
//
DELIMITER ;

