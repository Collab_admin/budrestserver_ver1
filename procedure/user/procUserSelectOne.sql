USE budcloud;
DROP PROCEDURE IF EXISTS procUserSelectOne;

DELIMITER //
CREATE PROCEDURE procUserSelectOne (
	IN $id INT
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	SELECT
		id,		
		user_id,			
		user_nm,			
		user_org,			
		user_pos,			
		user_mobile,			
		user_phone,
		pic_path,
		comp_key,			
		DATE_FORMAT(reg_date,'%Y-%m-%d %H:%i:%s') reg_date,
		DATE_FORMAT(upd_date,'%Y-%m-%d %H:%i:%s') upd_date
	FROM tb_m_user
	WHERE id=$id;	  
END;
//
DELIMITER ;

