USE budcloud;
DROP PROCEDURE IF EXISTS procUserUpdate;

DELIMITER //
CREATE PROCEDURE procUserUpdate (
	IN $id INT,
	IN $user_nm VARCHAR(50),
	IN $user_org VARCHAR(50),
	IN $user_pos VARCHAR(32),
	IN $user_mobile VARCHAR(15),
	IN $user_phone VARCHAR(20),
	IN $pic_path VARCHAR(200),
	IN $comp_key VARCHAR(100)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
	UPDATE tb_m_user SET
		user_nm=_user_nm,	  
		user_org=$user_org,
		user_pos=$user_pos,
		user_mobile=$user_mobile,  
		user_phone=$user_phone,
		pic_path=$pic_path,
		upd_date=NOW()
	WHERE id=$id
	AND comp_key=$comp_key;		
END;
//
DELIMITER ;
