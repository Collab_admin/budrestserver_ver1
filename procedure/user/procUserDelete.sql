USE budcloud;
DROP PROCEDURE IF EXISTS procUserDelete;

DELIMITER //
CREATE PROCEDURE procUserDelete (
	IN $id INT
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN	
	UPDATE tb_m_user SET
		user_id=CONCAT((SELECT user_id FROM tb_m_user WHERE id=$id), '-', NOW()),
		use_yn='N',  
		upd_date=NOW()
	WHERE id=$id;		
END;
//
DELIMITER ;

